import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    TouchableOpacity,
  } from "react-native";
  
  
  export default function Menu({navigation}) {
    return (
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.menu}
          onPress={() => navigation.navigate("PrimeiraPagina")}
        >
          <Text>Botão 1</Text>
        </TouchableOpacity>
  
        <TouchableOpacity
          style={styles.menu}
          onPress={() => navigation.navigate("SegundaPagina")}
          >
          <Text>Botão 2</Text>
        </TouchableOpacity>
  
        <TouchableOpacity style={styles.menu}
        onPress={() => navigation.navigate("TerceiraPagina")}
        >
          <Text>Botão 3</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.menu} 
        onPress={() => navigation.navigate("QuartaPagina")}
        >
          <Text>Botão 4</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.menu}
        onPress={() => navigation.navigate("QuintaPagina")}
        >
          <Text>Botão 5</Text>
        </TouchableOpacity>
      </View>
    );
  }
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
    menu: {
      padding: 10,
      margin: 5,
      backgroundColor: "pink",
      borderRadius: 5,
    },
  });
  