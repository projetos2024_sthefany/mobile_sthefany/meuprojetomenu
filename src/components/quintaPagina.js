import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  
} from "react-native";


export default function QuintaPagina({navigation}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("QuintaPagina")}
      >
        <Text>Botão 1</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  menu: {
    padding: 10,
    margin: 5,
    backgroundColor: "pink",
    borderRadius: 5,
  },
});
